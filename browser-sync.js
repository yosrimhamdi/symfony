const browserSync = require("browser-sync");

browserSync({
  files: "**/*",
  proxy: "http://127.0.0.1:8000/",
  notify: false,
  ignore: [
    "node_modules/*",
    "bin/*",
    "git/*",
    "tests/*",
    "*.yml",
    "vendor/*",
    "var/*",
  ],
});
