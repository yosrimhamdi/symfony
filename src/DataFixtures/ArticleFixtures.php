<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixtures extends Fixture {
  public function load(ObjectManager $manager): void {
    for ($i = 0; $i < 10; $i++) {
      $article = new Article();

      $article
        ->setTitle("title of Acticle Num: $i")
        ->setContent("content of Acticle Num: $i")
        ->setImage("http://placehold.it/350x150")
        ->setCreatedAt(new \DateTimeImmutable());

      $manager->persist($article);
    }

    $manager->flush();
  }
}
